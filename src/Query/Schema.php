<?php
namespace IMP\Query;

class Schema
{
	private static $cachedSchema = array();
	
	
	/**
	 * Try to generate the schema and then cache the results.
	 */
	private static function generateSchema($connection, $table)
	{
		// Extract the table name, in case the full DB.TABLE_SCHEMA.TABLE_NAME, or TABLE_SCHEMA.TABLE_NAME, format is used.
		// If a database name is specified in the "table" property, then override the name stored in the "pdo" property.
		list($databaseName, $tableName) = Util::parseTableString($table);
		// Make sure there is a proper PDO connection.
		$pdo = \IMP\PDO::connect($connection);
		// Get the default database name.
		if (empty($databaseName)) { $databaseName = $pdo->databaseName; }
		// Get the loaded SQL driver.
		$sqlDriver = $pdo->sqlDriverName;
		// Query the field properties for this model's table.
		$query = "
			SELECT c.COLUMN_NAME
				,cu.CONSTRAINT_NAME
				,(
					SELECT
					CASE WHEN cu.CONSTRAINT_NAME LIKE 'PK_%' OR cu.CONSTRAINT_NAME LIKE 'PRIMARY'
						THEN 1
						ELSE 0
					END
				) AS PK
				,c.DATA_TYPE
				,c.CHARACTER_MAXIMUM_LENGTH
				,c.NUMERIC_PRECISION
				,c.IS_NULLABLE
			" .($sqlDriver == 'mssql' ?
				",COLUMNPROPERTY(OBJECT_ID(c.TABLE_NAME), c.COLUMN_NAME, 'IsIdentity') AS AUTOINCREMENTS"
				:
				",IF(c.EXTRA LIKE '%auto_increment%', 1, 0) AS AUTOINCREMENTS"
			) . "
			FROM INFORMATION_SCHEMA.COLUMNS AS c
			LEFT JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS cu
				ON c.COLUMN_NAME = cu.COLUMN_NAME
					AND c.TABLE_NAME = cu.TABLE_NAME
					AND c.TABLE_SCHEMA = cu.TABLE_SCHEMA
			WHERE c.TABLE_NAME = :tableName
				AND (
						c.TABLE_SCHEMA = :databaseName1 /* MySQL */
						OR c.TABLE_CATALOG = :databaseName2 /* T-SQL */
					 )
		";
		$binds = array(
			':tableName'     => array('value' => $tableName,    'type' => \PDO::PARAM_STR),
			':databaseName1' => array('value' => $databaseName, 'type' => \PDO::PARAM_STR),
			':databaseName2' => array('value' => $databaseName, 'type' => \PDO::PARAM_STR),
		);
		// Create the prepared statement.
		$statement = $pdo->prepare($query);
		// Set up parameter binds.
		foreach ($binds as $param => $data) {
			$statement->bindValue($param, $data['value'], $data['type']);
		}
		// Execute the statement.
		try {
			if (method_exists($pdo, 'execute')) {
				$success = $pdo->execute($statement);
			} else {
				$success = $statement->execute();
			}
			if (!$success) {
				// Generate error message if not successful.
				$errorInfo = $statement->errorInfo();
				$error = 'SQL Error ('.$errorInfo[0].'): '.$errorInfo[2];
				trigger_error($error . "<br />\nQUERY:: $query<pre>BINDS:: " . print_r($binds, true) . "</pre>", E_USER_ERROR);
			}
		} catch (Exception $e) {
			print $e->getMessage()."\n".$e->getTraceAsString();
			$errorInfo = $statement->errorInfo();
			$error = 'SQL Error CAUGHT ('.$errorInfo[0].'): '.$errorInfo[2];
			trigger_error($error . "<br />\nQUERY:: $query<pre>BINDS:: " . print_r($binds, true) . "</pre>", E_USER_ERROR);
		}
		// Process the results.
		$schema = array();
		while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
			switch (strtolower($row['DATA_TYPE'])) {
				case 'int':
				case 'integer':
				case 'smallint':
				case 'tinyint':
				case 'mediumint':
				case 'bigint':
					$type = \IMP\Query::INT;
					break;
				case 'float':
				case 'decimal':
				case 'double':
					$type = \IMP\Query::FLOAT;
					break;
				case 'date':
					$type = \IMP\Query::DATE;
					break;
				case 'datetime':
				case 'timestamp':
					$type = \IMP\Query::DATETIME;
					break;
				case 'bit':
				case 'bool':
					$type = \IMP\Query::BOOL;
					break;
				default:
					$type = \IMP\Query::STR;
			}
			$field = array(
				'fieldName' => $row['COLUMN_NAME'],
				'type'      => $type,
			);
			// Check for max length.
			if ($row['CHARACTER_MAXIMUM_LENGTH'] > 0) { $field['maxLength'] = $row['CHARACTER_MAXIMUM_LENGTH']; }
			elseif ($row['NUMERIC_PRECISION'] > 0) { $field['maxLength'] = $row['NUMERIC_PRECISION']; }
			// Check for primary key.
			if ($row['PK']) { $field['isPrimaryKey'] = true; }
			// Check if required.
			if ($row['IS_NULLABLE'] === 'NO') { $field['isRequired'] = true; }
			// Check if the field autoincrements. If it is, also set the field as read-only.
			if ($row['AUTOINCREMENTS']) { $field['isAutoincrement'] = true; $field['isReadOnly'] = true; }
			// Generate the property name and label to use.
			$parts = preg_split('/(?<![A-Z])(?=[A-Z])|_|\s/', $field['fieldName']);
			foreach ($parts as $i => $v) { $parts[$i] = ucfirst($v); }
			$field['propertyName'] = implode('', $parts);
			$field['label'] = implode(' ', $parts);
			// Add this data to the schema.
			$schema[] = $field;
		}
		return $schema;
	}
	
	/**
	 * Initialize and get the schema.
	 */
	private static function initSchema($schema, $modelName)
	{
		// Make sure the schema is structured correctly.
		foreach ($schema as $index => $fieldData) {
			// The field name must be defined.
			if (!array_key_exists('fieldName', $fieldData)) {
				Util::error('No field specified for "' . $modelName .'" schema.');
				return false;
			}
			// The type must be defined.
			if (!array_key_exists('type', $fieldData)) {
				Util::error('No type specified for "' . $fieldData['fieldName'] . '" field in "'. $modelName.'" schema.');
				return false;
			}
			// If a property name and label is not specified, then generate each one.
			if (!array_key_exists('propertyName', $fieldData) || !array_key_exists('label', $fieldData)) {
				$parts = explode('_', $fieldData['fieldName']);
				foreach ($parts as $i => $v) { $parts[$i] = ucfirst($v); }
				if (!array_key_exists('label', $fieldData))        { $schema[$index]['label']        = implode(' ', $parts); }
				if (!array_key_exists('propertyName', $fieldData)) { $schema[$index]['propertyName'] = implode('', $parts); }
			}
			// Generate an alias for this field.
			if (!array_key_exists('fieldAlias', $fieldData)) {
				$schema[$index]['fieldAlias'] = $fieldData['fieldName'] . Indexer::getNextIndex();
			}
			// Initialize the value for this field-property.
			if (!array_key_exists('value', $fieldData)) {
				$schema[$index]['value'] = null;
			}
		}
		return $schema;
	}
	
	/**
	 * Get a schema.
	 */
	public static function getSchema($modelName, $alias = null, $forceRegeneration = false)
	{
		// Make sure there is an alias to store the schema under.
		if (empty($alias)) { $alias = $modelName; }
		// Check if there is already a cached schema.
		if (array_key_exists($alias, self::$cachedSchema) && !$forceRegeneration) {
			// Schema is already cached. Return cached schema.
			return self::$cachedSchema[$alias];
		} elseif (!empty($modelName::$schema)) {
			// Get the schema defined in the model definition.
			$schema = $modelName::$schema;
		} else {
			// Generate the schema.
			$schema = self::generateSchema($modelName::getConnection(), $modelName::getTable());
		}
		// Make sure the schema is properly structured.
		$schema = self::initSchema($schema, $modelName);
		// Check if the child class defined an initSchema() method.
		if (method_exists($modelName, 'initSchema')) {
			$schema = $modelName::initSchema($schema);
		}
		// Cache the schema.
		self::$cachedSchema[$alias] = $schema;
		// Output the newly cached schema.
		return $schema;
	}
}
?>