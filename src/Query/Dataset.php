<?php
namespace IMP\Query;

abstract class Dataset
{
	protected static $table = '';
	protected static $schema = '';
	protected static $connection = ''; // \IMP\PDO dependent connection name. See \IMP\PDO for more details.
	
	private $fields = array();
	private $virtualColumns = array();
	private $inputElements = array();
	
	public $alias = null;
	
	
	/**
	 * Static method: Create an instance of this model. Using this to create a new
	 * object ensures that it gets initialized correctly.
	 */
	public static function create($alias = null)
	{
		$obj = new static();
		$obj->alias = $alias;
		// Create an object property from the static schema.
		foreach (Schema::getSchema('\\' . get_called_class(), $obj->alias) as $fieldData) {
			$obj->fields[$fieldData['propertyName']] = $fieldData['value'];
			#$obj->inputElements[$fieldData['propertyName']] = $fieldData['inputElement'];
		}
		return $obj;
	}
	
	/**
	 * Basic way to get a field, or virtual column, value.
	 */
	public function retrieveValue($name)
	{
		if (isset($this->fields[$name])) { return $this->fields[$name]; }
		if (isset($this->virtualColumns[$name])) { return $this->virtualColumns[$name]; }
		return null;
	}
	
	/**
	 * Get private property.
	 */
	public static function getTable() { return static::$table; }
	public static function getConnection() { return static::$connection; }
	public function retrieveFields() { return $this->fields; }
	public function retrieveVirtualColumns() { return $this->virtualColumns; }
	
	/**
	 * Basic way to set a field value.
	 */
	public function assignValue($name, $value)
	{
		$schema = Schema::getSchema('\\' . get_called_class(), $this->alias);
		$found = false;
		foreach ($schema as $fieldData) {
			if ($fieldData['propertyName'] == $name) {
				$value = Util::typecastValue($value, $fieldData['type']);
				$this->fields[$name] = $value;
				$found = true;
				break;
			}
		}
		if (!$found) {
			$this->virtualColumns[$name] = $value;
		}
		return $value;
	}
	
	/**
	 * Basic way to set a virtual column.
	 */
	public function assignVirtualColumn($name, $value)
	{
		return $this->virtualColumns[$name] = $value;
	}
	
	/**
	 * Get the last query.
	 */
	public static function retrieveLastQuery()
	{
		return \IMP\Query::getLastQuery('\\' . get_called_class());
	}
	
	#public function retrieveFields() { return $this->fields; }
	#public function retrieveVirtualColumns() { return $this->virtualColumns; }
	
	/**
	 * Magic method:
	 *
	 * @param  String $name      - The name of the method called.
	 * @param  Array  $arguments - The arguments passed in the method in an array format.
	 *
	 * @return Mixed             - Depends on the called method.
	 */
	public function __call($methodName, $arguments)
	{
		if (substr($methodName, 0, 3) == 'get') {
			$name = substr($methodName, 3);
			return $this->retrieveValue($name);
			
		} elseif (substr($methodName, 0, 7) == 'display') {
			$name = substr($methodName, 7);
			$format = isset($arguments[0]) ? $arguments[0] : null; #<-- For displaying DateTime fields.
			return Util::encodeString(Util::convertToPrimitive($this->retrieveValue($name), $format));
			
		} elseif (substr($methodName, 0, 3) == 'set') {
			$name = substr($methodName, 3);
			$value = array_key_exists(0, $arguments) ? $arguments[0] : null;
			$this->assignValue($name, $value);
			return $this;
			
		} elseif (substr($methodName, 0, 15) == 'inputElementFor') {
			$name = substr($methodName, 15);
			if (isset($this->inputElements[$name])) { return $this->inputElements[$name]->setValue($this->fields[$name]); }
			else { return null; }
		} else {
			$this->error('Calling unknown method "' . $methodName .' " in "' . __CLASS__ . '" model.');
		}
	}
	
	/**
	 * Call the hydrator to hydrate this object using an array of values.
	 */
	public function hydrateFromArray($array)
	{
		Hydrator::hydrateFromArray($this, $array);
		return $this;
	}
	
	/**
	 * Call util method to get an array of primitive values.
	 */
	public function toArray($includeVirtualColumns = false, $includeProperties = false)
	{
		return Util::getFieldPrimitives($this, $includeVirtualColumns, $includeProperties);
	}

	/**
	 * Call INSERT or UPDATE depending on the values of the primary keys.
	 */
	public function doSave()
	{
		$schema = Schema::getSchema('\\' . get_called_class(), $this->alias);
		// Check if any primary key is empty.
		$doInsert = false;
		$pks = [];
		foreach ($schema as $fieldData) {
			if (!empty($fieldData['isPrimaryKey'])) {
				$pks[] = $fieldData['propertyName'];
				if (empty($this->{"get{$fieldData['propertyName']}"}())) {
					$doInsert = true;
				}
			}
		}
		$q = \IMP\Query::create(get_called_class())
			->addSet($this->toArray())
		;
		if ($doInsert) {
			$id = $q->doInsert();
		} elseif (empty($pks)) {
			$this->error('Calling doSave() for update but no primary keys found in "' . __CLASS__ . '" model.');
			return null;
		} else {
			$id = [];
			foreach($pks as $pk) {
				$id[] = $this->{"get$pk"}();
				$q->addWhere($pk, $this->{"get$pk"}());
			}
			$q->doUpdate();
			$id = implode('-', $id);
		}
		if (count($pks) == 1) {
			$this->{"set{$pks[0]}"}($id);
		}
		return $id;
	}
}
?>