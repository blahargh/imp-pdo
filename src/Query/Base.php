<?php
IMPQuery_Util
	*error($message)
	*expandFieldString($fieldString)
		parseFieldString()
		findModelData()
		findFieldData()
			findModelData()
	*parseTableString($tableString)
	*parseFieldString($fieldString)
	*typecastValue($value, $type)
	*convertToPrimitive($string)
	*getFieldPrimitives($object)
		getFieldValues()
		convertToPrimitive()
	*encodeString($string)
	*getFieldValues($object)
	*getParamValue()
	*getParamType()

IMPQuery_Base extends IMPQuery_Util
	query($modelName, $tableAlias = null)
		getConnection() -- model
	addSelect($fieldsCSV)
	addFrom($modelName, $tableAlias = null)
		parseTableString()
		initSchema()
		getTable() -- model
	addWhere($fieldString1, $fieldString2, $operator = 'LIKE')
	addOrderBy($fieldString, $sortOrder)
	useGroupBy($fieldsCSV)
	useLimit($number)
	useTop($number)
	addSet($fieldString, $value)
	addLeftJoin($modelName, $tableAlias = null)
		parseTableString()
		initSchema()
		getTable() -- model
	addOn($fieldString1, $fieldString2, $operator = '=')
	storeAs($storageType, $containerModelName = null, $containerModelPropertyName = null)
	useAlias($tableAlias)
	doCount()
	doSelect()
	doSelectOne()
	doInsert($useValues = array())
	doUpdate()
	doDelete()
	*getLastQuery()
	
	
IMPQuery_Schema
	generateSchema($modelName)
		parseTableString()
	initSchema()
		error()
	getSchema()
		getConnection() -- model
		getTable() -- model
	
IMPQuery_Generator
	generateCOUNTClause($settings)
	generateSELECTClause($settings)
		expandFieldString()
		encloseField()
	generateFROMClause($settings)
	generateWHEREClause($settings)
		expandFieldString()
		encloseField()
		generateConditionalSet()
	generateORDERBYClause($settings)
		expandFieldString()
		encloseField()
	generateGROUPBYClause($settings)
		expandFieldString()
		encloseField()
	generateLIMITClause($settings)
	generateJOINClause($settings)
		expandFieldString()
		encloseField()
		generateConditionalSet()
	generateINSERTClause($settings)
		expandFieldString()
	generateVALUESClause($settings)
		expandFieldString()
		convertToPrimitive()
		typecastValue()
	generateSETClause($settings)
		expandFieldString()
		encloseField()
		convertToPrimitive($value)
		typecastValue($value, $type)
	generateCountQuery($settings)
	generateSelectQuery($settings)
	generateInsertQuery($settings)
	generateUpdateQuery($settings)
	generateDeleteQuery($settings)
	translateQuery($query, $binds)
		getParamValue()
		getParamType()
	encloseField($string)
	generateConditionalSet($array)
	
IMPQuery_Execute
	executeQuery()
		error()
		getParamValue()
		getParamType()
	
IMPQuery_Hydrator
	generateDataID()
	removeMatchingFields()
	appendDataset()
		getFieldPrimitives()
		getFieldValues()
		hydrateFromArray()
	hydrateFromArray()
		typecastValue()
	hydrateToCollection()
	hydrateToObject()



IMPQuery::query($modelName, $alias = null)
	->addFrom($modelName, $alias = null)
	->doSelect()
;

list($query, $binds) = IMPQuery_Generator::generateSelectQuery($settings);
$pdoStatement = IMPQuery_Execute::executeQuery($query, $binds);
$collection = IMPQuery_Hydrator::hydrateToCollection($pdoStatement);

IMPQuery::$settings = array();


?>