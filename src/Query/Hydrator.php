<?php
namespace IMP\Query;

class Hydrator
{
	private $settings = null;
	
	public function __construct(&$settings)
	{
		$this->settings =& $settings;
	}
	
	
	/**
	 * Generate an ID for a dataset using the primary keys.
	 */
	private function generateDataID($dataset, $schema = null)
	{
		if (empty($schema)) { $schema = $this->settings['from']['schema']; }
		$values = array();
		foreach ($schema as $fieldData) {
			if (!empty($fieldData['isPrimaryKey'])) {
				if (isset($dataset[$fieldData['fieldAlias']])) { $values[] = $dataset[$fieldData['fieldAlias']]; }
				elseif (isset($dataset[$fieldData['fieldName']])) { $values[] = $dataset[$fieldData['fieldName']]; }
				elseif (isset($dataset[$fieldData['propertyName']])) { $values[] = $dataset[$fieldData['propertyName']]; }
			}
		}
		return implode('_', $values);
	}
	
	/**
	 * Remove fields in the dataset that exists in this model object.
	 */
	private function removeMatchingFields(&$dataset, $schema = null)
	{
		if (empty($schema)) { $schema = $this->settings['from']['schema']; }
		foreach ($schema as $i => $fieldData) {
			if (isset($dataset[$fieldData['fieldAlias']])) {       unset($dataset[$fieldData['fieldAlias']]); }
			elseif (isset($dataset[$fieldData['fieldName']])) {    unset($dataset[$fieldData['fieldName']]); }
			elseif (isset($dataset[$fieldData['propertyName']])) { unset($dataset[$fieldData['propertyName']]); }
		}
	}
	
	/**
	 * Compile a dataset into a collection array.
	 */
	private function appendDataset(&$collection, $dataset)
	{
		$usedFields = array();
		$objectPointer = array();
		$model = $this->settings['from']['modelName'];
		$mobj = $model::create($this->settings['from']['alias']);
		$mindex = $this->generateDataID($dataset) . '';
		if (!empty($mindex) && isset($collection[$mindex])) {
			// This dataset has already been collected. Attach submodels to the existing object.
			$mobj = $collection[$mindex];
			// No hydration needed, but matching fields needs to be removed so left over fields can be made into virtual columns.
			$this->removeMatchingFields($dataset);
		} else {
			$this->hydrateFromArray($mobj, $dataset);
			// Store the object in the collection if it's not yet in there.
			if (empty($mindex)) {
				$collection[] = $mobj;
				$mindex = count($collection) - 1;
			} else {
				$collection[$mindex] = $mobj;
			}
		}
		$objectPointer[$model] = $mobj;
		if (isset($this->settings['join'])) {
			foreach ($this->settings['join'] as $modelData) {
				$model = $modelData['modelName'];
				$sobj = $model::create($modelData['alias']);
				$property = empty($modelData['storeAs']['propertyName']) ? $modelData['modelName'] : $modelData['storeAs']['propertyName'];
				$container = empty($modelData['storeAs']['containerModel']) || !isset($objectPointer[$modelData['storeAs']['containerModel']]) ? $this->settings['from']['modelName'] : $modelData['storeAs']['containerModel'];
				$pobj = $objectPointer[$container];
				switch ($modelData['storeAs']['storageType']) {
					case 'ignore':
						$this->removeMatchingFields($dataset, $modelData['schema']);
						continue 2;
						break;
					case 'collection':
						$sindex = $this->generateDataID($dataset, $modelData['schema']) . '';
#print '<Pre>'.print_r($dataset, true).'</pre>';
#$class = '\\' . get_class($sobj);
#print '<pre>'.$class.'='.print_r(Schema::getSchema($class, $sobj->alias), true).'</pre>';
						$this->hydrateFromArray($sobj, $dataset);
						if (!isset($pobj->{$property})) { $pobj->{$property} = array(); }
						if (empty($sindex)) {
							// If there was no data at all, then don't add this object in the collection.
							if (implode('', Util::getFieldPrimitives($sobj)) == '') {
								// Empty object. Don't add to the collection.
							} else {
								$pobj->{$property}[] = $sobj;
							}
						} else {
							if (isset($pobj->{$property}[$sindex])) {
								$sobj = $pobj->{$property}[$sindex];
							} else {
								$pobj->{$property}[$sindex] = $sobj;
							}
						}
						break;
					case 'object':
						$this->hydrateFromArray($sobj, $dataset);
						$pobj->$property = $sobj;
						break;
					case 'virtualColumns':
					default:
						$this->hydrateFromArray($sobj, $dataset);
						foreach (Util::getFieldValues($sobj) as $n => $v) {
							$pobj->assignVirtualColumn($n, $v);
						}
				}
				$objectPointer[$model] = $sobj;
			}
		}
		// Add unused fields as the main object's virtual columns.
		foreach ($dataset as $field => $value) {
			$mobj->assignVirtualColumn($field, $value);
		}
		return $mindex;
	}
	
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- #
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- #
	
	/**
	 * Hydrate the object from a dataset array.
	 */
	public static function hydrateFromArray($obj, &$dataset, $unset = true)
	{
		$class = '\\' . get_class($obj);
		$schema = Schema::getSchema($class, $obj->alias);
		foreach ($schema as $fieldData) {
			if (array_key_exists($fieldData['fieldAlias'], $dataset)) {       $obj->assignValue($fieldData['propertyName'], Util::typecastValue($dataset[$fieldData['fieldAlias']], $fieldData['type']));   if ($unset) { unset($dataset[$fieldData['fieldAlias']]); } }
			elseif (array_key_exists($fieldData['fieldName'], $dataset)) {    $obj->assignValue($fieldData['propertyName'], Util::typecastValue($dataset[$fieldData['fieldName']], $fieldData['type']));    if ($unset) { unset($dataset[$fieldData['fieldName']]); } }
			elseif (array_key_exists($fieldData['propertyName'], $dataset)) { $obj->assignValue($fieldData['propertyName'], Util::typecastValue($dataset[$fieldData['propertyName']], $fieldData['type'])); if ($unset) { unset($dataset[$fieldData['propertyName']]); } }
		}
	}
	
	/**
	 * Use a PDOStatement object to hydrate a collection of objects.
	 */
	public function hydrateToCollection(\PDOStatement $statement = null)
	{
		$collection = array();
		if (!is_a($statement, '\PDOStatement')) { $statement = $this->statement; }
		while($dataset = $statement->fetch(\PDO::FETCH_ASSOC)) {
			$this->appendDataset($collection, $dataset);
		}
		return $collection;
	}
	
	/**
	 * Use a PDOStatement object to hydrate a single object.
	 */
	public function hydrateToObject(\PDOStatement $statement = null)
	{
		$mainIndex = null;
		$collection = array();
		if (!is_a($statement, '\PDOStatement')) { $statement = $this->statement; }
		while($dataset = $statement->fetch(\PDO::FETCH_ASSOC)) {
			$i = $this->appendDataset($collection, $dataset);
			if ($mainIndex === null) { $mainIndex = $i; }
			elseif ($mainIndex != $i) { break; }
		}
		$obj = reset($collection);
		if (empty($obj)) {
			// No data found. Return an empty main model object.
			$model = $this->settings['from']['modelName'];
			return $model::create($this->settings['from']['alias']);
		} else {
			return $obj;
		}
	}
}
?>