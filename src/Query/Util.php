<?php
namespace IMP\Query;

class Util
{
	/**
	 * Handles the error messages.
	 */
	public static function error($message)
	{
		// Halt on error is desired.
		#$backtrace = debug_backtrace();
		#print '<pre>' . print_r($backtrace, true). '</pre>';
		print '<pre>';
		throw new \Exception($message);
	}
	
	/**
	 * Encode output string to prevent some shenanigans.
	 */
	public static function encodeString($mixed)
	{
		$translation_table = array(
			 '<'  => '&lt;'
			,'>'  => '&gt;'
			,'®'  => '&reg;'
			,'™'  => '&trade;'
			,'©'  => '&copy;'
			,'|'  => '&#124;'
			,'"'  => '&quot;'
			,'“'  => '&ldquo;'
			,'”'  => '&rdquo;'
			,'`'	=> '&#096;'
			,"'"  => '&#039;'
		);
		if (is_array($mixed)) {
			foreach ($mixed as $i=>$v) {
				unset($mixed[$i]); #<-- Remove unencoded element.
				$mixed[self::encodeString($i)] = self::encodeString($v);
			}
		} else {
			$mixed = trim($mixed);
			$mixed = strtr($mixed, $translation_table);
			$mixed = htmlentities($mixed, ENT_SUBSTITUTE | ENT_QUOTES, 'UTF-8'); #<-- Encode all other characters that have HTML character entity equivalents, like the fraction "1/2".
			$mixed = str_replace('&amp;', '&', $mixed); #<-- Correct the "&" convertion done by htmlentities().
		}
		return $mixed;
	}
	
	/**
	 * Determine the actual value of a passed in parameter, in case it's an array that also contain information
	 * regarding the value, such as "type".
	 */
	public static function getParamValue($value)
	{
		if (is_array($value)) {
			if (isset($value['value'])) {
				return $value['value'];
			} else {
				// No "value" declared. Return a NULL.
				return null;
			}
		} else {
			return $value;
		}
	}
	
	/**
	 * Determine the PDO parameter type of a passed in parameter, in case it's an array that also contain
	 * information regarding the value, such as "type". If it doesn't, then consider it as string.
	 */
	public static function getParamType($value)
	{
		if (is_array($value)) {
			if (isset($value['type'])) {
				switch ($value['type']) {
					case \PDO::PARAM_BOOL:
					case \IMP\Query::BOOL:
						return \PDO::PARAM_BOOL;
						break;
					case \PDO::PARAM_INT:
					case \IMP\Query::INT:
					case \IMP\Query::FLOAT:
					case \IMP\Query::DECIMAL:
					case \IMP\Query::DOUBLE:
						return \PDO::PARAM_INT;
						break;
					case \PDO::PARAM_STR:
					case \IMP\Query::STR:
						return \PDO::PARAM_STR;
						break;
					case \PDO::PARAM_NULL:
					case \IMP\Query::NULL:
						// Non-matching string will end up here because \PDO::PARAM_NULL === 0, which "somestring" == 0 is TRUE. :/
						return \PDO::PARAM_NULL;
						break;
					default:
						return \PDO::PARAM_STR;
				}
			} else {
				// No "type" declared. Use this as a string.
				return \PDO::PARAM_STR;
			}
		} else {
			// Not an array with more information. Use this as a string.
			return \PDO::PARAM_STR;
		}
	}
	
	/**
	 * Get all related data from a field string.
	 */
	public static function expandFieldString($settings, $fieldString, $defaultModelAlias = '')
	{
		if (empty($fieldString)) { return false; }
		
		list ($modelString, $fieldString) = self::parseFieldString($fieldString, $defaultModelAlias);
		
		$modelData = array();
		if (isset($settings['from'])) {
			if (isset($settings['from']['alias']) && $settings['from']['alias'] == $modelString) { $modelData = $settings['from']; }
			if (isset($settings['from']['modelName']) && $settings['from']['modelName'] == $modelString) { $modelData = $settings['from']; }
		}
		if (isset($settings['join'])) {
			foreach ($settings['join'] as $mData) {
				if (isset($mData['alias']) && $mData['alias'] == $modelString) { $modelData = $mData; }
				if (isset($mData['modelName']) && $mData['modelName'] == $modelString) { $modelData = $mData; }
			}
		}
		if (empty($modelData)) { return false; }
		
		$fieldData = array();
		$schema = Schema::getSchema($modelData['modelName'], $modelData['alias']);
		foreach ($schema as $fData) {
			if ($fData['fieldName'] == $fieldString) { $fieldData = $fData; }
			if ($fData['fieldAlias'] == $fieldString) { $fieldData = $fData; }
			if ($fData['propertyName'] == $fieldString) { $fieldData = $fData; }
		}
		if (empty($fieldData)) { return false; }
		
		return array('modelString' => $modelString, 'fieldString' => $fieldString, 'modelData' => $modelData, 'fieldData' => $fieldData);
	}
	
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- #
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- #
	
	/**
	 * Extract the table alias and field name, in case the full TABLE_ALIAS.FIELD_NAME format is used.
	 */
	public static function parseFieldString($fieldString, $defaultTableAlias = '')
	{
		$fieldName = '';
		$modelData = array();
		$parts = explode('.', $fieldString);
		if (count($parts) == 2 && $parts[0]) {
			$tableAlias = trim($parts[0], '[]` ');
		} else {
			$tableAlias = $defaultTableAlias;
		}
		$fieldName = trim(array_pop($parts), '[]` ');
		return array($tableAlias, $fieldName);
	}
	
	/**
	 * Extract the database and table name, in case the full DB.TABLE_SCHEMA.TABLE_NAME, or TABLE_SCHEMA.TABLE_NAME, format is used.
	 */
	public static function parseTableString($tableString, $defaultDatabaseName = '')
	{
		$tableName = '';
		$parts = explode('.', $tableString);
		if (count($parts) == 3 && $parts[0]) {
			$databaseName = trim($parts[0], '[]` ');
		} else {
			$databaseName = $defaultDatabaseName;
		}
		$tableName = trim(array_pop($parts), '[]` ');
		return array($databaseName, $tableName);
	}
	
	/**
	 * Typecast a value based off of a type.
	 */
	public static function typecastValue($value, $type)
	{
		switch ($type) {
			case \PDO::PARAM_INT:
			case \IMP\Query::INT:
			case \IMP\Query::FLOAT:
				$value = +$value;
				break;
			case \PDO::PARAM_STR:
			case \IMP\Query::STR:
				$value = $value . '';
				break;
			case \IMP\Query::DATETIME:
				if ($value === null) {
					// Allow NULL value if there is no DateTime set.
				} elseif ($value === '') {
					// Treat empty strings as null.
					$value = null;
				} elseif (is_a($value, '\DateTime')) {
					// If the value is already a DateTime object, then store it as is.
					$value->impType = 'datetime';
				} else {
					// Convert to a DateTime object.
					// 32-bit systems has a limit to strtotime of Jan 19, 2038 03:14:07 UTC.
					$value = new \DateTime($value);
					$value->impType = 'datetime';
				}
				break;
			case \IMP\Query::DATE:
				if ($value === null) {
					// Allow NULL value if there is no DateTime set.
				} elseif ($value === '') {
					// Treat empty strings as null.
					$value = null;
				} elseif (is_a($value, '\DateTime')) {
					// If the value is already a DateTime object, then store it as is.
					$value->impType = 'date';
				} else {
					// Convert to a DateTime object.
					// 32-bit systems has a limit to strtotime of Jan 19, 2038 03:14:07 UTC.
					$value = new \DateTime($value);
					$value->impType = 'date';
				}
				break;
			case \PDO::PARAM_BOOL:
			case \IMP\Query::BOOL:
				if ($value + 0) {
					$value = true;
				} else {
					$lower = strtolower($value) . '';
					if ($lower == '1' || $lower == 'yes' || $lower == 'y' || $lower == 'on') {
						$value = true;
					} else {
						$value = false;
					}
				}
		}
		return $value;
	}
	
	/**
	 * Convert a value into a string if it's an object or an array.
	 */
	public static function convertToPrimitive($value, $format = null)
	{
		if (is_array($value)) {
			$output = json_encode($value);
		} elseif (is_a($value, '\DateTime')) {
			if (empty($format)) { $format = 'php'; }
			switch ($format) {
				case 'sql':
				case 'mysql':
				case 'big':
					$format = 'Y-m-d'; #<-- 2015-09-24
					break;
				case 'php':
				case 'middle':
					$format = 'm/d/Y'; #<-- 09/24/2015
					break;
				case 'little':
					$format = 'd M Y'; #<-- 24 Sep 2015
					break;
			}
			if (isset($value->impType)) {
				if ($value->impType == 'date') {
					$output = $value->format($format);
				} else {
					$output = $value->format("$format H:i:s");
				}
			} else {
				$output = $value->format("$format H:i:s");
			}
		} elseif (is_object($value) && method_exists($value, '__toString')) {
			$output = $value->__toString();
		} elseif (is_object($value)) {
			$output = '\\' . get_class($value) . ' object';
		} else {
			$output = $value;
		}
		return $output;
	}
	
	/**
	 * Get the values of all the fields in this object, converted to primitive values.
	 */
	public static function getFieldPrimitives($mixed, $includeVirtualColumns = false, $includeProperties = false)
	{
		$output = array();
		if (is_array($mixed)) {
			foreach ($mixed as $i => $v) {
				$output[$i] = self::getFieldPrimitives($v, $includeVirtualColumns, $includeProperties);
			}
		} elseif ($mixed instanceof Dataset) {
			$output = self::getFieldValues($mixed, $includeVirtualColumns, $includeProperties);
			foreach ($output as $n => $v) { $output[$n] = self::convertToPrimitive($v, 'sql'); }
		} else {
			$output = $mixed;
		}
		return $output;
	}
	
	/**
	 * Get the values of all the fields in this object.
	 */
	public static function getFieldValues($mixed, $includeVirtualColumns = false, $includeProperties = false)
	{
		$output = array();
		if (is_array($mixed)) {
			foreach ($mixed as $i => $v) {
				$output[$i] = self::getFieldValues($v, $includeVirtualColumns, $includeProperties);
			}
		} elseif ($mixed instanceof Dataset) {
			foreach ($mixed->retrieveFields() as $name => $value) {
				$output[$name] = $value;
			}
			if ($includeVirtualColumns) {
				foreach ($mixed->retrieveVirtualColumns() as $name => $value) {
					$output['(virtual) ' . $name] = $value;
				}
			}
			if ($includeProperties) {
				foreach (get_object_vars($mixed) as $name => $value) {
					if ($name == 'fields') { continue; }
					if ($name == 'virtualColumns') { continue; }
					if (is_array($value)) {
						foreach ($value as $n => $v) {
							if ($v instanceof Dataset) {
								if (!isset($output['(collection) ' . $name])) { $output['(collection) ' . $name] = array(); } #<-- Initialize here so an empty array is not displayed if this dataset is not requested.
								$output['(collection) ' . $name][$n] = self::getFieldValues($v, $includeVirtualColumns, $includeProperties);
							} else {
								if (!isset($output['(property) ' . $name])) { $output['(property) ' . $name] = array(); } #<-- Initialize here so an empty array is not displayed if this dataset is not requested.
								$output['(property) ' . $name][$n] = $v;
							}
						}
					} elseif ($value instanceof Dataset) {
						$output['(submodel) ' . $name] = self::getFieldValues($value, $includeVirtualColumns, $includeProperties);
					} else {
						$output['(property) ' . $name] = $value;
					}
				}
			}
		} else {
			$output = $mixed;
		}
		return $output;
	}
}
?>
