<?php
namespace IMP\Query;

class Generator
{
	private static $indentSpace = 2;         // The number of spaces to indent certain lines in the generated SQL statement.
	private $settings = null;
	private $binds = null;
	private $pdo = null;
	
	public function __construct(&$settings, &$binds, \PDO $pdo)
	{
		$this->settings =& $settings;
		$this->binds = & $binds;
		$this->pdo = $pdo;
	}
	
	
	/**
	 * Enclose an SQL field.
	 */
	private function encloseField($field)
	{
		switch ($this->pdo->sqlDriverName) {
			case 'mssql':
				return "[$field]";
				break;
			default:
				return '`' . $field . '`';
		}
		return $field;
	}
	
	/**
	 * Generate a conditional set.
	 */
	private function generateConditionalSet($conditions, $indent = 1)
	{
		/*
			->where("a", 23)
			->where('(', 'OR')
				->where("b", "c")
				->where("f", 55)
				->where('(')
					->where("d", 1)
					->where("e", 2)
				->where(')')
				->where("g", 23)
			->where(')')
			->where("h", 1002)
			
			"a" = 23
			AND (
				"b" = "c"
				OR "f" = 55
				OR (
					"d" = 1
					AND "e" = 2
				)
				OR "g" = 23
			)
			AND "h" = 1002
		*/
		$logic = '';
		$nextLogic = 'AND ';
		$compiled = '';
		foreach ($conditions as $condition) {
			if (is_array($condition)) {
				switch ($condition[0]) {
					case '(':
						$compiled .= "\n" . str_repeat(' ', self::$indentSpace * $indent) . "$logic(";
						if ($logic == '') { $logic = $nextLogic; }
						$prevLogic = $logic;
						$nextLogic = isset($condition[1]) && $condition[1] == 'OR' ? 'OR ' : 'AND ';
						$logic = '';
						$indent += 1;
						break;
					case ')':
						$indent -= 1;
						$compiled .= "\n" . str_repeat(' ', self::$indentSpace * $indent) . ")";
						$logic = $prevLogic;
						break;
				}
			} else {
				$compiled .= "\n" . str_repeat(' ', self::$indentSpace * $indent) . "$logic$condition";
				if ($logic == '') { $logic = $nextLogic; }
			}
		}
		return ltrim($compiled, "\n ");
	}
	
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- #
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- #
	
	/**
	 * Generate the COUNT clause.
	 */
	public function generateCOUNTClause()
	{
		return 'SELECT COUNT(*)';
	}
	
	/**
	 * Generate the SELECT clause.
	 */
	public function generateSELECTClause()
	{
		$selectToUse = array();
		$data = !empty($this->settings['join']) ? $this->settings['join'] : array();
		$data[] = $this->settings['from'];
		foreach ($data as $modelData) {
			if (isset($modelData['select'])) {
				foreach ($modelData['select'] as $selectData) {
					$fields = explode(',', $selectData['fieldsCSV']);
					foreach ($fields as $fieldString) {
						$data = Util::expandFieldString($this->settings, $fieldString, $modelData['alias']);
						if ($data) {
							// Field name.
							$selectToUse[] = "{$data['modelData']['alias']}.{$this->encloseField($data['fieldData']['fieldName'])} AS {$data['fieldData']['fieldAlias']}";
						} elseif ($fieldString == '*') {
							// * - all fields.
							foreach ($modelData['schema'] as $fieldData) {
								$selectToUse[] = "{$modelData['alias']}.{$this->encloseField($fieldData['fieldName'])} AS {$fieldData['fieldAlias']}";
							}
						} else {
							// Literal string.
							$selectToUse[] = $fieldString;
						}
					}
				}
			}
		}
		if (count($selectToUse) == 0) {
			// If the SELECT clause was not specified, then select all fields.
			foreach ($this->settings as $clause => $data) {
				if ($clause == 'from') { $this->settings[$clause]['select'][] = array('fieldsCSV' => '*'); }
				if ($clause == 'join') { foreach ($data as $i => $modelData) { $this->settings[$clause][$i]['select'][] = array('fieldsCSV' => '*'); } }
			}
			return $this->generateSELECTClause();
		}
		return 'SELECT ' . ($this->pdo->sqlDriverName == 'mssql' && $this->settings['limit'] != '' ? "TOP $this->settings['limit'] " : '') . implode(', ', $selectToUse);
	}
	
	/**
	 * Generate the FROM clause.
	 */
	public function generateFROMClause()
	{
		return "\nFROM {$this->settings['from']['table']} AS {$this->settings['from']['alias']}";
	}
	
	/**
	 * Generate the WHERE clause.
	 */
	public function generateWHEREClause()
	{
		$whereToUse = array();
		$data = !empty($this->settings['join']) ? $this->settings['join'] : array();
		$data[] = $this->settings['from'];
		foreach ($data as $modelData) {
			if (isset($modelData['where'])) {
				foreach ($modelData['where'] as $whereData) {
					$where = '';
					if ($whereData['field1'] == '(') { $whereToUse[] = array('(', $whereData['field2']); continue; }
					if ($whereData['field1'] == ')') { $whereToUse[] = array(')');                       continue; }
					if ($data1 = Util::expandFieldString($this->settings, $whereData['field1'], $modelData['alias'])) {
						// Field name.
						$where .= "{$data1['modelData']['alias']}.{$this->encloseField($data1['fieldData']['fieldName'])}";
					} else {
						// Literal string.
						$param = ':pb' . Indexer::getNextIndex();
						$where .= $param;
						$this->binds[$param] = $whereData['field1'];
					}
					$where .= " {$whereData['operator']} ";
					if ($data2 = Util::expandFieldString($this->settings, $whereData['field2'], $this->settings['from']['alias'])) {
						// Field name.
						$where .= "{$data2['modelData']['alias']}.{$this->encloseField($data2['fieldData']['fieldName'])}";
					} elseif ($whereData['operator'] == 'IN' || $whereData['operator'] == 'NOT IN') {
						// CSV.
						$values = explode(',', $whereData['field2']);
						$list = array();
						foreach ($values as $v) {
							$v = trim($v);
							if ($v == '') { continue; }
							$param = ':pb' . Indexer::getNextIndex();
							$list[] = $param;
							$this->binds[$param] = array('value' => $v, 'type' => $data1 ? $data1['fieldData']['type'] : \IMP\Query::STR);
						}
						$where .= '(' . implode(', ', $list) . ')';
					} elseif (!empty($whereData['field2'])) {
						// Literal string.
						$param = ':pb' . Indexer::getNextIndex();
						$where .= $param;
						$this->binds[$param] = array('value' => $whereData['field2'], 'type' => $data1 ? $data1['fieldData']['type'] : \IMP\Query::STR);
					}
					$whereToUse[] = $where;
				}
			}
		}
		if (count($whereToUse) > 0) {
			return "\nWHERE " . $this->generateConditionalSet($whereToUse);
		} else {
			return '';
		}
	}
	
	/**
	 * Generate the ORDER BY clause.
	 */
	public function generateORDERBYClause()
	{
		$orderByToUse = array();
		$data = !empty($this->settings['join']) ? $this->settings['join'] : array();
		$data[] = $this->settings['from'];
		foreach ($data as $modelData) {
			if (isset($modelData['orderBy'])) {
				foreach ($modelData['orderBy'] as $orderByData) {
					if ($data = Util::expandFieldString($this->settings, $orderByData['field'], $modelData['alias'])) {
						$orderByToUse[] = "{$data['modelData']['alias']}.{$this->encloseField($data['fieldData']['fieldName'])} " . (strtolower($orderByData['sort']) == 'desc' ? 'DESC' : 'ASC');
					}
				}
			}
		}
		if (count($orderByToUse) > 0) {
			return "\nORDER BY " . implode(",\n" . str_repeat(' ', self::$indentSpace), $orderByToUse);
		} else {
			return '';
		}
	}
	
	/**
	 * Generate the GROUP BY clause.
	 */
	public function generateGROUPBYClause()
	{
		$groupByToUse = array();
		$data = !empty($this->settings['join']) ? $this->settings['join'] : array();
		$data[] = $this->settings['from'];
		foreach ($data as $modelData) {
			if (isset($modelData['groupBy'])) {
				foreach ($modelData['groupBy'] as $groupByData) {
					$fields = explode(',', $groupByData['fieldsCSV']);
					foreach ($fields as $fieldString) {
						if ($data = Util::expandFieldString($this->settings, trim($fieldString), $modelData['alias'])) {
							$groupByToUse[] = "{$data['modelData']['alias']}.{$this->encloseField($data['fieldData']['fieldName'])}";
						}
					}
				}
			}
		}
		if (count($groupByToUse) > 0) {
			return "\nGROUP BY " . implode(",\n" . str_repeat(' ', self::$indentSpace), $groupByToUse);
		} else {
			return '';
		}
	}
	
	/**
	 * Generate the LIMIT clause.
	 */
	public function generateLIMITClause()
	{
		if (isset($this->settings['limit'])) { ##Comment this out so it accepts "3, 1" limit format --> && is_numeric($this->settings['limit'])) {
			return "\nLIMIT {$this->settings['limit']}";
		} else {
			return '';
		}
	}
	
	/**
	 * Generate the JOIN clause.
	 */
	public function generateJOINClause()
	{
		if (isset($this->settings['join'])) {
			$joinToUse = '';
			foreach ($this->settings['join'] as $modelData) {
				$onToUse = array();
				foreach ($modelData['on'] as $onData) {
					$on = '';
					if ($onData['field1'] == '(') { $onToUse[] = array('(', $onData['field2']); continue; }
					if ($onData['field1'] == ')') { $onToUse[] = array(')');                    continue; }
					if ($data1 = Util::expandFieldString($this->settings, $onData['field1'], $modelData['alias'])) {
						// Field name.
						$on .= "{$data1['modelData']['alias']}.{$this->encloseField($data1['fieldData']['fieldName'])}";
					} else {
						// Literal value.
						$param = ':pb' . Indexer::getNextIndex();
						$on .= $param;
						$this->binds[$param] = $onData['field1'];
					}
					$on .= " {$onData['operator']} ";
					if ($data2 = Util::expandFieldString($this->settings, $onData['field2'], $this->settings['from']['alias'])) {
						// Field name.
						$on .= "{$data2['modelData']['alias']}.{$this->encloseField($data2['fieldData']['fieldName'])}";
					} else {
						// Literal value.
						$param = ':pb' . Indexer::getNextIndex();
						$on .= $param;
						$this->binds[$param] = array('value' => $onData['field2'], 'type' => $data1 ? $data1['fieldData']['type'] : \IMP\Query::STR);
					}
					$onToUse[] = $on;
				}
				$joinToUse .= "\n" . strtoupper($modelData['type']) . " JOIN {$modelData['table']} AS {$modelData['alias']}"
					."\n" . str_repeat(' ', self::$indentSpace) . "ON " . $this->generateConditionalSet($onToUse, 2)
				;
			}
			return $joinToUse;
		} else {
			return '';
		}
	}
	
	/**
	 * Generate the INSERT clause.
	 */
	public function generateINSERTClause($useFields = array())
	{
		$fieldsToUse = array();
		if (count($useFields) > 0) {
			// Specify the fields to use.
			foreach ($useFields as $field) {
				if ($data = Util::expandFieldString($this->settings, $field, $this->settings['from']['alias'])) {
					$fieldsToUse[] = $this->encloseField($data['fieldData']['fieldName']);
				}
			}
		} else {
			// Use the fields in the generated query.
			$data = !empty($this->settings['join']) ? $this->settings['join'] : array();
			$data[] = $this->settings['from'];
			foreach ($data as $modelData) {
				if (isset($modelData['set'])) {
					foreach ($modelData['set'] as $setData) {
						if ($data = Util::expandFieldString($this->settings, $setData['field'], $modelData['alias'])) {
							$fieldsToUse[] = $this->encloseField($data['fieldData']['fieldName']);
						}
					}
				}
			}
		}
		return 'INSERT INTO ' . $this->settings['from']['table']
			."\n(\n" . str_repeat(' ', self::$indentSpace) . implode(",\n" . str_repeat(' ', self::$indentSpace), $fieldsToUse) . "\n)"
		;
	}
	
	/**
	 * Generate the VALUES clause.
	 */
	public function generateVALUESClause()
	{
		$valuesToUse = array();
		$data = !empty($this->settings['join']) ? $this->settings['join'] : array();
		$data[] = $this->settings['from'];
		foreach ($data as $modelData) {
			if (isset($modelData['set'])) {
				foreach ($modelData['set'] as $setData) {
					if ($data = Util::expandFieldString($this->settings, $setData['field'], $modelData['alias'])) {
						$param = ':pb' . Indexer::getNextIndex();
						$valuesToUse[] = $param;
						$this->binds[$param] = array('value' => Util::convertToPrimitive(Util::typecastValue($setData['value'], $data['fieldData']['type']), 'mysql'), 'type' => $data['fieldData']['type']);
					}
				}
			}
		}
		if (count($valuesToUse) > 0) {
			return "\nVALUES (\n" . str_repeat(' ', self::$indentSpace) . implode(",\n" . str_repeat(' ', self::$indentSpace), $valuesToUse) . "\n)";
		} else {
			return '';
		}
	}
	
	/**
	 * Generate the SET clause.
	 */
	public function generateSETClause()
	{
		$setToUse = array();
		$data = !empty($this->settings['join']) ? $this->settings['join'] : array();
		$data[] = $this->settings['from'];
		foreach ($data as $modelData) {
			if (isset($modelData['set'])) {
				foreach ($modelData['set'] as $setData) {
					if ($data = Util::expandFieldString($this->settings, $setData['field'], $modelData['alias'])) {
						$param = ':pb' . Indexer::getNextIndex();
						$setToUse[] = "{$data['modelData']['alias']}.{$this->encloseField($data['fieldData']['fieldName'])} = $param";
						$this->binds[$param] = array('value' => Util::convertToPrimitive(Util::typecastValue($setData['value'], $data['fieldData']['type']), 'mysql'), 'type' => $data['fieldData']['type']);
					}
				}
			}
		}
		if (count($setToUse) > 0) {
			return "\nSET \n" . str_repeat(' ', self::$indentSpace) . implode(",\n" . str_repeat(' ', self::$indentSpace), $setToUse);
		} else {
			return '';
		}
	}
	
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- #
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- #
	
	
}
?>