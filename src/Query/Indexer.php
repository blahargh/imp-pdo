<?php
namespace IMP\Query;

class Indexer
{
	private static $index = 1;
	
	/**
	 * Get a number and advance it by 1.
	 */
	public static function getNextIndex()
	{
		return self::$index++;
	}
}
?>