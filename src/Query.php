<?php
namespace IMP;

class Query
{
	protected static $lastQuery = array();     // Store the last query generated.
	
	private $pdo = null;                     // The PDO connection object of the main model.
	private $settings = array();             // An array of all the query settings declared since the last call to query().
	private $currentModelSettings = null;    // A pointer that points to the last attached model settings array.
	private $query = '';
	private $binds = array();
	private $statement = null;
	
	const INTEGER  = 'BMQTYPE1';
	const INT      = 'BMQTYPE1';
	const STRING   = 'BMQTYPE2';
	const STR      = 'BMQTYPE2';
	const BOOL     = 'BMQTYPE3';
	const BOOLEAN  = 'BMQTYPE3';
	const NULL     = 'BMQTYPE4';
	const DATE     = 'BMQTYPE5';
	const DATETIME = 'BMQTYPE6';
	const FLOAT    = 'BMQTYPE7';
	const DECIMAL  = 'BMQTYPE7';
	const DOUBLE   = 'BMQTYPE7';
	
	const DATE_SQL     = 'Y-m-d';
	const DATE_PHP     = 'm/d/Y';
	const DATETIME_SQL = 'Y-m-d H:i:s';
	const DATETIME_PHP = 'm/d/Y H:i:s';
	
	public function __construct(\PDO $pdo)
	{
		$this->pdo = $pdo;
		$this->queryGenerator = new Query\Generator($this->settings, $this->binds, $this->pdo);
		$this->queryHydrator = new Query\Hydrator($this->settings);
	}
	
	
	/**
	 * Translate query.
	 */
	public static function translateQuery($query, $binds = array())
	{
		$translation = array();
		foreach ($binds as $param => $value) {
			switch (Query\Util::getParamType($value)) {
				case \PDO::PARAM_BOOL:
				case \PDO::PARAM_INT:
					$translation[$param] = Query\Util::getParamValue($value);
					break;
				case \PDO::PARAM_NULL:
					$translation[$param] = 'NULL';
					break;
				default:
					$translation[$param] = "'" . Query\Util::getParamValue($value) . "'";
			}
		}
		return strtr($query, $translation);
	}
	
	
	
	/**
	 * Get the last query generated.
	 */
	public static function getLastQuery($modelName)
	{
		if (substr($modelName, 0, 1) !== '\\') { $modelName = '\\' . $modelName; }
		if (isset(self::$lastQuery[$modelName])) { return self::$lastQuery[$modelName]; }
		else { return null; }
	}
	
	/**
	 * Start a query.
	 */
	public static function create($modelName, $alias = null)
	{
		if (substr($modelName, 0, 1) !== '\\') { $modelName = '\\' . $modelName; }
		$queryObj = new self(\IMP\PDO::connect($modelName::getConnection()));
		$queryObj->settings['query'] = array('modelName' => $modelName);
		$queryObj->useFrom($modelName, $alias);
		return $queryObj;
	}
	/*
	No point allowing a PDO object to be passed in since the other parts of \IMP\Query, such as \IMP\Query\Schema requires
	an \IMP\PDO connection name. See \IMP\PDO for more info.
	public static function query($modelName, $aliasOrPDO = null, $pdo = null)
	{
		if (substr($modelName, 0, 1) !== '\\') { $modelName = '\\' . $modelName; }
		if (!empty($aliasOrPDO)) {
			if ($aliasOrPDO instanceof \PDO) {
				$alias = null;
				$pdo = $aliasOrPDO;
			} elseif (is_string($aliasOrPDO)) {
				$alias = $aliasOrPDO;
			} else {
				$alias = null;
			}
		} else {
			$alias = null;
		}
		if (!($pdo instanceof \PDO)) {
			if (!empty($modelName::getConnection()) && $pdo = \IMP\PDO::connect($modelName::getConnection())) {
			} else {
				$pdo = null;
			}
		}
		$queryObj = new self($pdo);
		$queryObj->settings['query'] = array('modelName' => $modelName);
		$queryObj->useFrom($modelName, $alias);
		return $queryObj;
	}
	*/
	
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- #
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- #
	
	/**
	 * Add for the SELECT clause.
	 */
	public function addSelect($fieldsCSV)
	{
		if (!isset($this->currentModelSettings['select'])) { $this->currentModelSettings['select'] = array(); }
		$this->currentModelSettings['select'][] = array('fieldsCSV' => $fieldsCSV);
		return $this;
	}
	
	/**
	 * Add for the FROM clause.
	 */
	public function useFrom($modelName, $alias = null)
	{
		list($databaseName, $tableName) = Query\Util::parseTableString($modelName::getTable());
		if (empty($alias)) { $alias = $tableName . Query\Indexer::getNextIndex(); }
		$this->settings['from'] = array('modelName' => $modelName, 'schema' => Query\Schema::getSchema($modelName, $alias), 'alias' => $alias, 'table' => $modelName::getTable(), 'databaseName' => $databaseName, 'tableName' => $tableName);
		$this->currentModelSettings =& $this->settings['from'];
		return $this;
	}
	
	/**
	 * Add a WHERE clause.
	 */
	public function addWhere($field1, $field2 = null, $operator = 'LIKE')
	{
		if (!isset($this->currentModelSettings['where'])) { $this->currentModelSettings['where'] = array(); }
		$this->currentModelSettings['where'][] = array('field1' => $field1, 'field2' => $field2, 'operator' => $operator);
		return $this;
	}
	
	/**
	 * Add ORDER BY clause.
	 */
	public function addOrderBy($field, $sort = 'asc')
	{
		if (!isset($this->currentModelSettings['orderBy'])) { $this->currentModelSettings['orderBy'] = array(); }
		$this->currentModelSettings['orderBy'][] = array('field' => $field, 'sort' => $sort);
		return $this;
	}
	
	/**
	 * Set GROUP BY clause.
	 */
	public function useGroupBy($fieldsCSV)
	{
		if (!isset($this->currentModelSettings['groupBy'])) { $this->currentModelSettings['groupBy'] = array(); }
		$this->currentModelSettings['groupBy'][] = array('fieldsCSV' => $fieldsCSV);
		return $this;
	}
	
	/**
	 * Set LIMIT clause.
	 */
	public function useLimit($number)
	{
		$this->settings['limit'] = $number;
		return $this;
	}
	
	/**
	 * Set TOP clause.
	 */
	public function useTop($number)
	{
		return $this->limit($number);
	}
	
	/**
	 * Add SET clause.
	 */
	public function addSet($field, $value = null)
	{
		if (!isset($this->currentModelSettings['set'])) { $this->currentModelSettings['set'] = array(); }
		if (is_array($field)) {
			foreach ($field as $f => $v) {
				$this->currentModelSettings['set'][] = array('field' => $f, 'value' => $v);
			}
		} else {
			$this->currentModelSettings['set'][] = array('field' => $field, 'value' => $value);
		}
		return $this;
	}
	
	/**
	 * Add a LEFT JOIN clause.
	 */
	public function addLeftJoin($modelName, $alias = null)
	{
		if (!isset($this->settings['join'])) { $this->settings['join'] = array(); }
		list($databaseName, $tableName) = Query\Util::parseTableString($modelName::getTable());
		if (empty($alias)) { $alias = $tableName . Query\Indexer::getNextIndex(); }
		$index = array_push($this->settings['join'], array('type' => 'left', 'modelName' => $modelName, 'schema' => Query\Schema::getSchema($modelName, $alias), 'alias' => $alias, 'storeAs' => array('storageType' => 'virtualColumns', 'propertyName' => null, 'containerModel' => null), 'table' => $modelName::getTable(), 'databaseName' => $databaseName, 'tableName' => $tableName)) - 1;
		$this->currentModelSettings =& $this->settings['join'][$index];
		return $this;
	}
	
	/**
	 * Add INNER JOIN clause.
	 */
	public function addInnerJoin($modelName, $alias = null)
	{
		if (!isset($this->settings['join'])) { $this->settings['join'] = array(); }
		list($databaseName, $tableName) = Query\Util::parseTableString($modelName::getTable());
		if (empty($alias)) { $alias = $tableName . Query\Indexer::getNextIndex(); }
		$index = array_push($this->settings['join'], array('type' => 'inner', 'modelName' => $modelName, 'schema' => Query\Schema::getSchema($modelName, $alias), 'alias' => $alias, 'storeAs' => array('storageType' => 'virtualColumns', 'propertyName' => null, 'containerModel' => null), 'table' => $modelName::getTable(), 'databaseName' => $databaseName, 'tableName' => $tableName)) - 1;
		$this->currentModelSettings =& $this->settings['join'][$index];
		return $this;
	}
	
	/**
	 * Add an ON clause.
	 */
	public function addOn($field1, $field2, $operator = '=')
	{
		if (!isset($this->currentModelSettings['on'])) { $this->currentModelSettings['on'] = array(); }
		$this->currentModelSettings['on'][] = array('field1' => $field1, 'field2' => $field2, 'operator' => $operator);
		return $this;
	}
	
	/**
	 * Specify how to store a joined dataset.
	 * $storage = 'virtualColumns' | 'object' | 'collection'
	 */
	public function storeAs($storageType, $propertyName = null, $containerModel = null)
	{
		$this->currentModelSettings['storeAs'] = array('storageType' => $storageType, 'propertyName' => $propertyName, 'containerModel' => $containerModel);
		return $this;
	}
	
	/**
	 * Set the table alias, or recreate a table alias for this model object.
	 */
	public function useAlias($tableAlias)
	{
		$this->currentModelSettings['alias'] = $tableAlias;
		return $this;
	}
	
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- #
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- #
	
	/**
	 * Create a COUNT statement.
	 */
	public function doCount($execute = true)
	{
		$this->query = $this->queryGenerator->generateCOUNTClause()
			.$this->queryGenerator->generateFROMClause()
			.$this->queryGenerator->generateJOINClause()
			.$this->queryGenerator->generateWHEREClause()
			.$this->queryGenerator->generateLIMITClause()
		;
		
		if ($execute) {
			$this->executeStatement();
			return $this->statement->fetchColumn();
		} else {
			// Store this query as the last query used.
			self::$lastQuery[$this->settings['query']['modelName']] = self::translateQuery($this->query, $this->binds);
			return self::$lastQuery[$this->settings['query']['modelName']];
		}
	}
	
	/**
	 * Create a collection from the results of a SELECT statement.
	 */
	public function doSelect($execute = true)
	{
		$this->query = $this->queryGenerator->generateSELECTClause()
			.$this->queryGenerator->generateFROMClause()
			.$this->queryGenerator->generateJOINClause()
			.$this->queryGenerator->generateWHEREClause()
			.$this->queryGenerator->generateORDERBYClause()
			.$this->queryGenerator->generateLIMITClause()
		;
		
		if ($execute) {
			$this->executeStatement();
			return $this->queryHydrator->hydrateToCollection($this->statement);
		} else {
			// Store this query as the last query used.
			self::$lastQuery[$this->settings['query']['modelName']] = self::translateQuery($this->query, $this->binds);
			return self::$lastQuery[$this->settings['query']['modelName']];
		}
	}
	
	/**
	 * Create a single main model object from the results of a SELECT statement.
	 */
	public function doSelectOne($execute = true)
	{
		$this->query = $this->queryGenerator->generateSELECTClause()
			.$this->queryGenerator->generateFROMClause()
			.$this->queryGenerator->generateJOINClause()
			.$this->queryGenerator->generateWHEREClause()
			.$this->queryGenerator->generateORDERBYClause()
			.$this->queryGenerator->generateLIMITClause()
		;
		
		if ($execute) {
			$this->executeStatement();
			return $this->queryHydrator->hydrateToObject($this->statement);
		} else {
			// Store this query as the last query used.
			self::$lastQuery[$this->settings['query']['modelName']] = self::translateQuery($this->query, $this->binds);
			return self::$lastQuery[$this->settings['query']['modelName']];
		}
	}
	
	/**
	 * Insert data into the database using the generated SQL statement.
	 */
	public function doInsert($valueSets = array(), $execute = true)
	{
		if (count($valueSets) > 0) {
			// Multiple inserts.
			$query = $this->queryGenerator->generateINSERTClause(array_keys(reset($valueSets)));
			$query .= "\nVALUES ";
			foreach ($valueSets as $set) {
				$values = array();
				foreach ($set as $name => $value) {
					$param = ':pb' . self::getAliasNum();
					$values[] = $param;
					$this->binds[$param] = $value;
				}
				$query .= "\n(" . implode(', ', $values) . ")";
			}
		} else {
			// Single insert.
			$query = $this->queryGenerator->generateINSERTClause()
				.$this->queryGenerator->generateVALUESClause()
			;
		}
		$this->query = $query;
		
		if ($execute) {
			$this->executeStatement();
			return $this->pdo->lastInsertId();
		} else {
			// Store this query as the last query used.
			self::$lastQuery[$this->settings['query']['modelName']] = self::translateQuery($this->query, $this->binds);
			#return self::$lastQuery[$this->settings['query']['modelName']];
			return null;
		}
	}
	
	/**
	 * Execute an UPDATE statement.
	 */
	public function doUpdate($execute = true)
	{
		$whereClause = $this->queryGenerator->generateWHEREClause();
		if (empty($whereClause)) {
			Query\Util::error('UPDATE statement requested without a WHERE clause.');
			return;
		}
		if ($this->pdo->sqlDriverName == 'mssql') {
			// MSSQL syntax.
			$this->query = 'UPDATE ' . $this->settings['from']['alias']
				.$this->queryGenerator->generateSETClause()
				.$this->queryGenerator->generateFROMClause()
				.$this->queryGenerator->generateJOINClause()
				.$whereClause
			;
		} else {
			// MySQL syntax.
			$this->query = "UPDATE {$this->settings['from']['table']} AS {$this->settings['from']['alias']}"
				.$this->queryGenerator->generateJOINClause()
				.$this->queryGenerator->generateSETClause()
				.$whereClause
			;
		}
		
		if ($execute) {
			$this->executeStatement();
		} else {
			// Store this query as the last query used.
			self::$lastQuery[$this->settings['query']['modelName']] = self::translateQuery($this->query, $this->binds);
			return self::$lastQuery[$this->settings['query']['modelName']];
		}
	}
	
	/**
	 * Execute a DELETE statement.
	 */
	public function doDelete($execute = true)
	{
		$whereClause = $this->queryGenerator->generateWHEREClause();
		if (empty($whereClause)) {
			Query\Util::error('DELETE statement requested without a WHERE clause.');
			return;
		}
		$this->query = 'DELETE ' . $this->settings['from']['alias']
			.$this->queryGenerator->generateFROMClause()
			.$this->queryGenerator->generateJOINClause()
			.$whereClause
		;
		
		if ($execute) {
			$this->executeStatement();
		} else {
			// Store this query as the last query used.
			self::$lastQuery[$this->settings['query']['modelName']] = self::translateQuery($this->query, $this->binds);
			return self::$lastQuery[$this->settings['query']['modelName']];
		}
	}
	
	/**
	 * Execute an SQL statement.
	 */
	public function executeStatement($query = null, $binds = array())
	{
		if (empty($query)) { $query = $this->query; }
		if (empty($binds)) { $binds = $this->binds; }
		
		// Remove indentation on the query string for easier debugging.
		$query = ltrim($query, "\r\n");
		$indentation = strspn($query, " \t");
		if ($indentation > 0) { $query = str_replace(substr($query, 0, $indentation), '', $query); }
		
		// Store this query as the last query used.
		self::$lastQuery[$this->settings['query']['modelName']] = self::translateQuery($query, $binds);
		
		// Make sure the defined PDO connection is valid.
		if (empty($this->pdo) || !is_object($this->pdo) || !is_a($this->pdo, '\PDO')) {
			Query\Util::error('Model object "' . $this->settings['query']['modelName'] . '" does not contain a PDO connection.');
			return false;
		} else {
			$pdo = $this->pdo;
		}
		
		// Create the prepared statement.
		$statement = $pdo->prepare($query);
		
		// Set up parameter binds.
		foreach ($binds as $param => $value) {
			$statement->bindValue($param, Query\Util::getParamValue($value), Query\Util::getParamType($value));
		}
		
		// Execute the statement.
		try {
			if (method_exists($pdo, 'execute')) {
				$success = $pdo->execute($statement);
			} else {
				$success = $statement->execute();
			}
			if (!$success) {
				// Generate error message if not successful.
				$errorInfo = $statement->errorInfo();
				$error = 'SQL Error ('.$errorInfo[0].'): '.$errorInfo[2];
				Query\Util::error($error . "<br />\nQUERY:: $query<pre>BINDS:: " . print_r($binds, true) . "</pre>");
			} else {
				$this->statement = $statement;
				return $this;
			}
		} catch (\Exception $e) {
			print $e->getMessage()."\n".$e->getTraceAsString();
			$errorInfo = $statement->errorInfo();
			$error = 'SQL Error CAUGHT ('.$errorInfo[0].'): '.$errorInfo[2];
			Query\Util::error($error . "<br />\nQUERY:: $query<pre>BINDS:: " . print_r($binds, true) . "</pre>");
		}
	}
	
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- #
	# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- #
	
	/**
	 * Call the hydrator.
	 */
	public function hydrateToCollection(\PDOStatement $statement = null)
	{
		if (!is_a($statement, '\PDOStatement')) { $statement = $this->statement; }
		return $this->queryHydrator->hydrateToCollection($statement);
	}
	
	/**
	 * Call the hydrator.
	 */
	public function hydrateToObject(\PDOStatement $statement = null)
	{
		if (!is_a($statement, '\PDOStatement')) { $statement = $this->statement; }
		return $this->queryHydrator->hydrateToObject($statement);
	}
}
?>
