<?php
namespace IMP;

/**
 * Class definition that will create an object that will handle
 * database connection using the PHP Data Objects (PDO) extension.
 */
class PDO
{
	private static $config = array();
	private static $dbConnections = array();
	
	/**
	 * Set the config to the connection repository. The format is compatible with the way .ini files can be parsed, using parse_ini_file().
	 * 	Format:
	 * 		$config = [
	 * 			'connection1' => ['db.connection1.dsn' => 'mysql:host=127.0.0.1;dbname=testdb',            'db.connection1.user' => 'blah', 'db.connection1.pass' => 'secretugh'],
	 * 			'connection2' => ['db.connection2.dsn' => 'pgsql:host=127.0.0.1;port=5432;dbname=testdbA', 'db.connection2.user' => 'argh', 'db.connection2.pass' => 'secretgrrr'],
	 * 		];
	 */
	public static function setConfig(array $config)
	{
		self::$config = $config;
	}
	
	/**
	 * Add a DB connection setting that can be used to connect to later.
	 * @param String $dsn - The Data Source Name, or DSN, contains the information
	 * 	required to connect to the database. See http://php.net/manual/en/pdo.construct.php
	 * 	for more details.
	 * 	Example:
	 * 		'mysql:host=127.0.0.1;dbname=testdb'
	 * 		'pgsql:host=127.0.0.1;port=5432;dbname=testdb'
	 * 		'Sqlsrv:server=127.0.0.1;database=testdb'
	 * @param String $user - The user name for the DB connection.
	 * @param String $pass - The password for the DB connection.
	 */
	public static function addConfig($name, $dsn, $user, $pass)
	{
		self::$config = array(
			"db.$name.dsn" => $dsn,
			"db.$name.user" => $user,
			"db.$name.pass" => $pass,
		);
	}
	
	/**
	 * Static method to create and retrieve an SQL connection. If the name passed in already
	 * exists, then it will return the pre-existing connection to minimize the number of
	 * SQL connections running at the same time.
	 *
	 * @param String $name - The name associated with a specific SQL connection. The connection
	 *   information needs to be set in the config file as:
	 *     db.<name>.dsn  = <dsn>
	 *     db.<name>.user = <db_user>
	 *     db.<name>.pass = <db_password>
	 *
	 * @return Mixed where IMPPDO object if the connection is successful, or FALSE otherwise.
	 */
	public static function connect($name, $dsn = null, $user = null, $pass = null)
	{
		if (func_num_args() == 1) {
			if (empty(self::$config["db.$name.dsn"])) { return false; }
			if (empty(self::$config["db.$name.user"])) { return false; }
			if (empty(self::$config["db.$name.pass"])) { return false; }
			$dsn = self::$config["db.$name.dsn"];
			$user = self::$config["db.$name.user"];
			$pass = self::$config["db.$name.pass"];
		}
		if (empty(self::$dbConnections[$name])) {
			// Connection not yet initialized.
			// Initialize and cache the connection.
			try {
				$pdo = new \PDO($dsn, $user, $pass);
			} catch (PDOException $e) {
				trigger_error("Unable to connect to the DB. See IMP\PDO.<br />\n<i>" . $e->getMessage() . '</i>', E_USER_ERROR);
			}
			// Determine the SQL driver, and therefore, the syntax to use when generating a query.
			$driver = $pdo->getAttribute(\PDO::ATTR_DRIVER_NAME);
			switch ($driver) {
				case 'sqlsrv':
				case 'mssql':
				case 'tsql':
					$pdo->sqlDriverName = 'mssql';
					break;
				default:
					$pdo->sqlDriverName = 'mysql';
			}
			// Get the name of the database.
			$query = 'SELECT ' . ($pdo->sqlDriverName == 'mssql' ? 'DB_NAME()' : 'DATABASE()');
			$database = $pdo->query($query)->fetchColumn();
			$pdo->databaseName = $database;
			// Get the server version.
			$pdo->serverVersion = $pdo->getAttribute(\PDO::ATTR_SERVER_VERSION);
			// Cache the connection.
			self::$dbConnections[$name] = $pdo;
		}
		// Return the cached connection.
		return self::$dbConnections[$name];
	}
}
?>